# 3D Viewer
## Description
Application for viewing 3D wireframe models.
- The program is developed in C++ language standard c++17
- The program provides the opportunity to:
    - Load a wireframe model from an obj file (vertices and surfaces list support only)
    - Translate the model by a given distance in relation to the X, Y, Z axes
    - Rotate the model by a given angle in relation to its X, Y, Z axes
    - Scale the model by a given value
    - Selecting the projection type (parallel and central)
    - Selecting line type (solid, dotted)
    - Selecting line color
    - Selecting Vertex Size
    - Selecting a background color
    - Auto-saving user settings
    - Taking screenshots of the visualization window
    - Creating screencasts - recording current affine transformations loaded into GIF animation (640x480, 10 fps, 5 sec.)
- The program is implemented using the MVC pattern
- The GIU is based on QT6

## Interface
![image](/materials/1.png)